<nav>
    <ul class="main-menu">
        <% loop $SiteSettings.MainMenu.MenuItems %>
            <% if $Children.count %>
                <li><a href="#">$Title</a>
                    <ul class="dropdown">
                        <% loop $Children %>
                            <li><a href="$Link">$Title</a></li>
                        <% end_loop %>
                    </ul>
                </li>
            <% else %>
                <li <% if isCurrent %>class="active"<% end_if %>>
                    <a href="$Link">$Title</a>
                </li>
            <% end_if %>
        <% end_loop %>
    </ul>
</nav>