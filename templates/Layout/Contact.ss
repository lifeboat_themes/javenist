<section class="heading-banner-area pt-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="heading-banner">
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="/">Home</a><span class="breadcome-separator">></span></li>
                            <li>Contact</li>
                        </ul>
                    </div>
                    <div class="heading-banner-title">
                        <h1>Contact us</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact-form-area mt-20">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="contact-form">
                    <form id="contact-form" action="mail.php" method="post">
                        <div class="contact-input">
                            <div class="first-name">
                                <input type="text" name="first_name" placeholder="First Name *"/>
                            </div>
                            <div class="last-name">
                                <input type="text" name="last_name" placeholder="Last Name *"/>
                            </div>
                            <div class="email">
                                <input type="email" name="email" placeholder="Email *"/>
                            </div>
                        </div>
                        <div class="contact-message mb-20">
                            <div class="message pr-10 pr-xs-0">
                                <textarea name="message" cols="40" rows="10" placeholder="Message *"></textarea>
                            </div>
                        </div>
                        <div class="contact-submit">
                            <button type="submit" class="form-button">Send Email</button>
                        </div>
                    </form>
                    <p class="form-messege"></p>
                </div>
            </div>
            <!--Contact Form End-->
            <!--Contact Address Start-->
            <div class="col-lg-5">
                <div class="contact-address-info">
                    <div class="contact-form-title">
                        <h2>Get in touch</h2>
                    </div>
                    <% if $ContactPage.Address || $ContactPage.Tel || $ContactPage.Email %>
                        <div class="contact-address mb-35">
                            <ul>
                                <% if $ContactPage.Address %><li><i class="fa fa-fax"></i> Address: $ContactPage.Address</li><% end_if %>
                                <% if $ContactPage.Tel %><li><i class="fa fa-phone"></i> Telephone number: $ContactPage.Tel</li><% end_if %>
                                <% if $ContactPage.Email %><li><i class="fa fa-envelope-o"></i> Email: $ContactPage.Email</li><% end_if %>
                            </ul>
                        </div>
                    <% end_if %>
                    <% if $ContactPage.OpeningHours %>
                        <div class="woring-hours mb-35">
                            <h3><strong>Working hours</strong></h3>
                            <p>$ContactPage.OpeningHours</p>
                        </div>
                    <% end_if %>
                </div>
            </div>
            <!--Contact Address End-->
        </div>
    </div>
</section>