<!--

    This template is used to render the auth pages customers interact with:
    Login, Register, Forgot Password, Verification, etc.

    Simply call $Form, Lifeboat renders the appropriate form
    based on the action performed by the user.
-->